pengan
======

A python script that calculates compound interest on investments. 

## Usage

The script can be run in two ways.

- a) See how much monthly investment becomes over time

- b) See how much you need to invest to reach a certain amount of money

Enter the values it asks for, if fields are left empty it runs with example values.

For example, you put 1000€ into an investment account that on average gives 5% annual
interest. Additionally you'll add 100€ per month for 10 years.
So if you started in 2021 you should have 16533.24€ in 2031.

```
$ pengan
+-+-+-+-+-+-+
|p|e|n|g|a|n|
+-+-+-+-+-+-+
compound interest calculator

a) How much does a monthly investment become in x amount of years?
b) How long does it take to get a certain sum by investing?

Choose a or b: a

Starting amount (empty = 0€): 1000
Monthly sum (empty = 500€): 100
Years (empty = 30): 10
Average interest rate (empty = 5%): 5

After 10 years you should in total have: ~16533.24€.
Which means that in the year 2033 you'll get a monthly average of ~68.88€ in interest.
```

## Installation

Clone repo, cd into it and install using make:

``` sh
git clone https://codeberg.org/rl/pengan
cd pengan
make install
```

## Troubleshooting

### If you can't run the program

By default it will install the script to `~/.local/bin/` so either edit the Makefile or add this to your `~/.bashrc` or similar if you can not run the script without specifying its full path:

``` ~/.bashrc
export PATH=$PATH:$HOME/.local/bin
```


[Formulas this script is based on]: https://www.thecalculatorsite.com/articles/finance/compound-interest-formula.php
