"""
+-+-+-+-+-+-+
|p|e|n|g|a|n|
+-+-+-+-+-+-+
compound interest calculator
https://codeberg.org/rl/pengan
"""

import math
from datetime import date


def initial_compound(initialsum, interestrate, years, months=12):
    """
    Function that returns the total sum of
    the starting amount with interest over time.
    """
    calc = initialsum * (1 + (interestrate * 0.01) * 0.01
                         / months) ** (months * years)
    return calc


def regular_compound(monthlypayment, interestrate, years, months=12):
    """
    Function that returns sum of regular
    savings with interest over time.
    """
    calc = monthlypayment * (((1 + (interestrate * 0.01)
                               / months) ** (months * years) - 1)
                             / ((interestrate * 0.01) / months))
    return calc


def find_monthly(monthly, years, interestrate, months=12):
    """
    Function that returns total sum of
    regular savings with interest over time.
    """
    return monthly / (((1 + (interestrate * 0.01) / months) ** (months * years)
                       - 1) / ((interestrate * 0.01) / months))


def interest_monthly(desiredamount, interestrate):
    """
    Function that returns interest per month on desired sum.
    """
    return (desiredamount * (interestrate * 0.01)) / 12


def truncate(number, digits) -> float:
    """
    Function that round numbers at two decimals.
    """
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


def find_year(amount_of_years):
    """
    Function that returns desired year.
    """
    return str(date.today().year + amount_of_years)


def banner():
    """
    Function that prints banner and explains options.
    """
    banner_content = """+-+-+-+-+-+-+
|p|e|n|g|a|n|
+-+-+-+-+-+-+
compound interest calculator

a) How much does a monthly investment become in x amount of years?
b) How long does it take to get a certain sum by investing?
"""
    print(banner_content)


def option_a():
    """"
    How much does a monthly investment become in certain amount of years?
    """
    starting_sum = float(input('\nStarting amount (empty = 0€): ') or "0")
    monthly_sum = float(input('Monthly sum (empty = 500€): ') or "500")
    years = int(input('Years (empty = 30): ') or "30")
    interestrate = float(input("Average interest rate (empty = 5%): ") or "5")
    years_found = find_year(years)

    value = (initial_compound(starting_sum, interestrate, years)) + (
        regular_compound(monthly_sum, interestrate, years))

    print("\nAfter " + str(years) + " years you should in total have: ~"
          + str(truncate(value, 2)) + "€.\nWhich means that in the year "
          + years_found + " you'll get a monthly average of ~" +
          str(truncate(interest_monthly(value, interestrate), 2))
          + "€ in interest.")


def option_b():
    """
    How long does it take to get a certain sum by investing?
    """
    desire = float(input('\nDesired amount (empty = 1000000€): ') or 1000000)
    years = int(input('Years (empty = 30): ') or 30)
    interestrate = float(input('Average interest rate (empty = 5%) ') or 5)
    years_found = find_year(years)

    print("\nYou would need to invest " +
          str(truncate(find_monthly(desire, years, interestrate), 2))
          + "€/month, then you should have " + str(desire)
          + "€ by the year " + years_found
          + ". \nThe interest rate should give you an average of ~" +
          str(truncate(interest_monthly(desire, interestrate), 2))
          + "€/month.")


def main():
    """
    Main function prints the banner and provides given option.
    """
    banner()
    answer = input("Choose a or b: ")

    if answer == "a" or answer == "A":
        option_a()
    elif answer == "b" or answer == "B":
        option_b()
    else:
        print("You didn't choose a valid option, exiting...")


if __name__ == "__main__":
    main()
