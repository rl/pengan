##
# pengan
#
# @file
# @version 0.1

all: install

install:
	pip install .

remove: uninstall

uninstall:
	pip uninstall pengan

clean:
	rm -rf venv
	rm -rf build
	rm -rf src/pengan.egg-info
	rm -rf src/pengan/__pycache__

virtual: init_venv venv_deps
	PYTHONPATH=venv
	@printf "\n\nVirtual environment setup. All you need to do now is run:\nsource ./venv/bin/activate\n"

init_venv:
	if [ ! -e "venv/bin/activate" ] ; then PYTHONPATH=venv ; virtualenv --clear venv ; fi

venv_deps:
	PYTHONPATH=venv ; . venv/bin/activate && pip install -e .

.PHONY: all install remove uninstall clean virtual init_venv venv_deps

# end
